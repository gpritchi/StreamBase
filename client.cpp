/*
The client should be able to connect to the server through a NamedPipe - Done
The client should be able to make both sync and async calls to the server
The client should be able to send trivial data (strings, numbers) to the server
The client should be able to create objects on the server (based on req-7 below), retrieve them, their attributes and call methods on them
The server should be able to receive both sync/async connection requests from clients
The server should be able to store data provided by the client via NamedPipe in a reasonable data structure
The server should be able to register a custom class (w/ related functions, attributes) which can be used by the client (see req-4)
The server should be able to store the custom objects created by the client for the custom class created in req-7
*/

#include <windows.h> 
// #include <iostream>

int main(void)
{
    HANDLE hPipe;
    DWORD dwWritten;

    hPipe = CreateFile(TEXT("\\\\.\\pipe\\MyNamedPipe"), 
                       GENERIC_READ | GENERIC_WRITE, 
                       0,
                       NULL,
                       OPEN_EXISTING,
                       0,
                       NULL);
    if (hPipe != INVALID_HANDLE_VALUE)
    {
        WriteFile(hPipe,
                  "Hello Pipe\n",
                  12,   // = length of string + terminating '\0' !!!
                  &dwWritten,
                  NULL);

        CloseHandle(hPipe);
    }

    return (0);
}

int syncPipeCall(void){
    
    return(0);
}